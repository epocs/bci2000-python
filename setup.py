#!/usr/bin/env -S python #
# -*- coding: utf-8 -*-

# ///////////////////////////////////////////////////////////////////////////
# $Id: $
# Author: jeremy.hill@neurotechcenter.org
# Description: Python pip/setuptools helper file for installing tools/python suite
#
# $BEGIN_BCI2000_LICENSE$
#
# This file is part of BCI2000, a platform for real-time bio-signal research.
# [ Copyright (C) 2000-2022: BCI2000 team and many external contributors ]
#
# BCI2000 is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# BCI2000 is distributed in the hope that it will be useful, but
#                         WITHOUT ANY WARRANTY
# - without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $END_BCI2000_LICENSE$
# ///////////////////////////////////////////////////////////////////////////

"""
The purpose of this file is to make the contents of the neighboring `python`
subdirectory available to your Python interpreter (i.e. listed on its
`sys.path`) in any future Python session.

The recommended way to use this file is to open a command prompt, change
your working directory to the location of this `setup.py` file, and then::

    python -m pip install -e .

The "editable" flag `-e` causes Python's setuptools to simply store a link
to the present location of the `python` subdirectory. The advantage of
an editable installation is that you still have only one definitive copy
of the code (presumably under version control) and any update to those
files will then be adopted automatically following a new Python launch.
The downside is that Python will fail to find the files if you ever move
the version-controlled files away from their current location.

Remove the `-e` flag if you want setuptools to make a static *copy* of
the modules/packages from the `python` subdirectory, within the
`site-packages` of your Python distribution or in your virtual environment.
This installation method makes it harder to upgrade code, unless we are
also managing and distributing our code via pypi.org (which we currently
do not intend to do).
"""

import os
import sys
import inspect
import setuptools

package_dir = 'python' # the directory that would get added to the path, expressed relative to the location of this setup.py file

try: __file__
except: __file__ = inspect.getfile( inspect.currentframe() )
HERE = os.path.dirname( __file__ )

# import the package itself - but make sure it's the to-be-installed version and not some legacy version hanging over
#sys.path.insert( 0, os.path.join( HERE, package_dir ) )
#import BCI2000Tools  # could use this to get the __version__ etc
#sys.path.pop( 0 )

setup_args = dict(
    name='bci2000-python',
    package_dir={ '' : package_dir },
    packages=[ 'BCI2000Tools' ],
    install_requires=[ 'numpy' ],
)

if __name__ == '__main__':
	if len( sys.argv ) < 2: # called without any subcommand---would normally prompt print-usage-and-exit
		default_subcmd = [ 'install', '-e', HERE ]
		sys.argv += default_subcmd
		print( 'Assuming subcommand:  ' + ' '.join( ( '"%s"' % arg ) if ' ' in arg else arg for arg in default_subcmd ) )
	setuptools.setup( **setup_args )
